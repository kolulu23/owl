package cn.tongdun.owl.benchmark

/**
 *
 * @author liutianlu
 * <br/>Created 2022/7/21 2:36 PM
 */
class Temp {
    Object run(Map<String, Object> inputParamMap) {
        Double a = ((Long) inputParamMap.get("@m1")).doubleValue();
        Long b = (Long) inputParamMap.get("@m2");
        Object m3 = inputParamMap.get("@m3");
        Double c = m3 == null ? 0.0 : (Double) m3;
        if (b == 0) {
            return false;
        } else {
            return (a / b) >= 0.6 && (b >= 40) && (c >= 100);
        }
    }
}
